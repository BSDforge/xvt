#include "xvtlib.h"

/*----------------------------------------------------------------------*/
/* main() */
/* INTPROTO */
int
main(int argc, const char *const *argv)
{
    xvt_t         *xvt_vars;

    if ((xvt_vars = xvt_init(argc, argv)) == NULL)
	return EXIT_FAILURE;
    xvt_main_loop(xvt_vars);	/* main processing loop */
    return EXIT_SUCCESS;
}
